    <section id="tentang_kami" class="home-section paddingtop-40">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="callaction bg-gray">
              <div class="row">
                <div class="col-md-8">
                  <div class="wow fadeInUp" data-wow-delay="0.1s">
                    <div class="cta-text"><br/><br/>
                      <h3>Tentang Kami</h3>
                      <p>
					  {foreach from=$tentang_kami item=i}
						  {$i.description}
					  {/foreach}
					  
                      </p>
                    </div>
                  </div>
                </div>
         
              </div>
			  
			 <div class="row">
                <div class="col-md-8">
                  <div class="wow fadeInUp" data-wow-delay="0.1s">
                    <div class="cta-text">
                      <h3>Maksud dan Tujuan</h3>
                      <p>
					  {foreach from=$maksud_tujuan item=i}
						  {$i.description}
					  {/foreach}
         
                       </p>
                    </div>
                  </div>
                </div>
         
              </div>
			  
			  
            </div>
          </div>
        </div>
      </div>
    </section>