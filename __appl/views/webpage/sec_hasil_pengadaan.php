
	 <!-- css -->
  <link href="{$host}__assets/css1/bootstrap-responsive.css" rel="stylesheet" />
  <link href="{$host}__assets/css1/style.css" rel="stylesheet" />
  
  <br><br><br><br><br>
<section id="content">
      <div class="container">
        <div class="row marginbot30">
          <div class="span12">
           <div class="wow fadeInUp" data-wow-delay="0.1s">
            <h4 class="heading"><strong>Hasil</strong> Pengadaan<span></span></h4>
            <div class="row">
              <div class="span6">
                <div class="wrapper">
                  <div class="testimonial">
                    <div class="author">
                      <h5>Non KHS</h5>
                        <table width="" class="table" border="0">
						<tbody>
							{foreach from=$hasil_pengadaan item=i}
							<tr>	
								<td><strong>Wilayah *</strong></td>
								<td style="font-weight:bold;" colspan="3">
									<select name="nama" id="nama" class="form-control" style="width:%">
										<option value="">-- Pilih --</option>
										<option>{$i.tipe_perusahaan}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><input type="date" name="tanggal_awal" value="Tanggal Awal" class="form-control" style="width:%"/></td>
								<td colspan="3"><input type="date" name="tanggal_akhir" value="Tanggal Akhir" class="form-control" style="width:%"/></td>
							</tr>
							</tr>
							<tr>
								<td><input name="tanggal_akhir" placeholder="Cari Data" class="form-control" style="width:%" /></td>
								<td><input type="submit" value="&ensp;Cari&ensp;" class="btn btn-info" style="width:100%"/></td>
								<td><input type="reset" value="Reset" class="btn btn-info" style="width:100%"/></td>
							</tr>
							{/foreach}
						</tbody>
					  </table>
					  
                      </p>
                     </div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="wrapper">
                  <div class="testimonial">
				  <h5>KHS</h5>
                        <table width="" class="table" border="0">
						<tbody>
							{foreach from=$hasil_pengadaan item=i}
							<tr>	
								<td><strong>Wilayah *</strong></td>
								<td style="font-weight:bold;" colspan="3">
									<select name="nama" id="nama" class="form-control" style="width:%">
										<option value="">-- Pilih --</option>
										<option>{$i.tipe_perusahaan}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><input type="date" name="tanggal_awal" value="Tanggal Awal" class="form-control" style="width:%"/></td>
								<td colspan="3"><input type="date" name="tanggal_akhir" value="Tanggal Akhir" class="form-control" style="width:%"/></td>
							</tr>
							</tr>
							<tr>
								<td><input name="tanggal_akhir" placeholder="Cari Data" class="form-control" style="width:%" /></td>
								<td><input type="submit" value="&ensp;Cari&ensp;" class="btn btn-info" style="width:100%"/></td>
								<td><input type="reset" value="Reset" class="btn btn-info" style="width:100%"/></td>
							</tr>
							{/foreach}
						</tbody>
					  </table>
					    </p>
                     </div>
                  </div>
                </div>
              </div>
            <div class="row">
              <div class="span6">
                <div class="wrapper">
                  <div class="testimonial">
					{foreach from=$tentang_kami item=i}
                      <i class="icon-quote-left icon-48"></i>
						  {$i.description}
                    <div class="author">
                      <img src="img/dummies/testimonial-author1.png" class="img-circle bordered" alt="" />
                      <p class="name">
                        Tom Shaun Dealova
                      </p>
                      <span class="info">Eproc PT JMTO, <a href="#">www.eproc.jmto.co.id</a></span>
					  {/foreach}
                    </div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="wrapper">
                  <div class="testimonial">
					{foreach from=$tentang_kami item=i}
                      <i class="icon-quote-left icon-48"></i>
						  {$i.description}
                    <div class="author">
                      <img src="img/dummies/testimonial-author1.png" class="img-circle bordered" alt="" />
                      <p class="name">
                        Tom Shaun Dealova
                      </p>
                      <span class="info">Eproc PT JMTO, <a href="#">www.eproc.jmto.co.id</a></span>
					  {/foreach}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="span6">
                <div class="wrapper">
                  <div class="testimonial">
					{foreach from=$tentang_kami item=i}
                      <i class="icon-quote-left icon-48"></i>
						  {$i.description}
                    <div class="author">
                      <img src="img/dummies/testimonial-author1.png" class="img-circle bordered" alt="" />
                      <p class="name">
                        Tom Shaun Dealova
                      </p>
                      <span class="info">Eproc PT JMTO, <a href="#">www.eproc.jmto.co.id</a></span>
					  {/foreach}
                    </div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="wrapper">
                  <div class="testimonial">
					{foreach from=$tentang_kami item=i}
                      <i class="icon-quote-left icon-48"></i>
						  {$i.description}
                    <div class="author">
                      <img src="img/dummies/testimonial-author1.png" class="img-circle bordered" alt="" />
                      <p class="name">
                        Tom Shaun Dealova
                      </p>
                      <span class="info">Eproc PT JMTO, <a href="#">www.eproc.jmto.co.id</a></span>
					  {/foreach}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </table>