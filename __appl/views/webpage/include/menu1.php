    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="top-area">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-6">
              <p class="bold text-left" >&nbsp;&ensp;&ensp;E-Procurement</p>
            </div>
            <div class="col-sm-6 col-md-6">
              <p class="bold text-right" id="watch">&nbsp;</p>
            </div>
          </div>
        </div>
      </div>
      <div class="container navigation">

        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
          <a class="navbar-brand" href="<?php echo base_url();?>">
                    <img src="{$host}__assets/webpage/img/logoeproc.png" alt="" width="220" height="45" />
                </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">
                <li><a href="{$host}">Beranda</a></li>
			<li class="dropdown">
              <a href="#"class="active" class="dropdown-toggle" data-toggle="dropdown">Konten Spesial <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{$host}webpage/syarat">Syarat dan Ketentuan</a></li>
                <li><a href="{$host}webpage/buku">Buku Panduan</a></li>
                <li><a href="{$host}webpage/dokumen">Dokumen</a></li>
              </ul>
            </li>

			<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Keanggotaan <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{$host}login" target="_blank">Log In</a></li>
                <li><a href="{$host}webpage/reg" target="_blank">Registrasi</a></li>
              </ul>
            </li>


					<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pengumuman <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{$host}webpage/pengumuman_pengadaan">Pengumuman Pengadaan</a></li>
                <li><a href="{$host}webpage/pengumuman_dpt">Pengumuman DPT</a></li>
                <li><a href="{$host}webpage/hasil_pengadaan">Hasil Pengadaan</a></li>
                <li><a href="{$host}webpage/hasil_dpt">Hasil DPT</a></li>
                <li><a href="{$host}webpage/berita">Berita</a></li>
              </ul>
            </li>

          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container -->
    </nav>
