<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Webpage extends CI_Controller {

	function __construct() {
		parent::__construct();
	   $this->host	= $this->config->item('base_url');
	   $this->smarty->assign('host',$this->host);
	   $this->load->library(array('form_validation','encrypt'));
	   $this->load->helper(array('captcha','form'));
	   $this->load->model("mmodul");
	   $this->smarty->assign("acak", md5(date('H:i:s')));
	   $this->captcha = $this->session->userdata('captcha');
  }

	public function index()
	{
		
		$tentang_kami=$this->mmodul->getdata ("tentang_kami", "result_array");
		$this->smarty->assign("tentang_kami",$tentang_kami);
		
		$maksud_tujuan=$this->mmodul->getdata ("maksud_tujuan", "result_array");
		$this->smarty->assign("maksud_tujuan",$maksud_tujuan);
		
		$faq=$this->mmodul->getdata ("faq", "result_array");
		$this->smarty->assign("faq",$faq);
		
		
		$this->smarty->display('webpage/tpl_home.php');
	}
	public function syarat()
	{
		
		$syarat_ketentuan=$this->mmodul->getdata ("syarat_ketentuan", "result_array");
		$this->smarty->assign("syarat_ketentuan",$syarat_ketentuan);
		
		$this->smarty->display('webpage/syarat.php');
	}
	
	public function buku()
	{
		$panduan=$this->mmodul->getdata ("panduan", "result_array");
		$this->smarty->assign("panduan",$panduan);
		
		$this->smarty->display('webpage/buku.php');
	}
	
	public function dokumen()
	{
		$tentang_kami=$this->mmodul->getdata ("tentang_kami", "result_array");
		$this->smarty->assign("tentang_kami",$tentang_kami);
		
		$dokumen=$this->mmodul->getdata ("dokumen", "result_array");
		$this->smarty->assign("dokumen",$dokumen);
		
		$this->smarty->display('webpage/dokumen.php');
	}
	
	public function pengumuman_pengadaan()
	{
		$tentang_kami=$this->mmodul->getdata ("tentang_kami", "result_array");
		$this->smarty->assign("tentang_kami",$tentang_kami);
		
		$pengumuman_pengadaan=$this->mmodul->getdata ("pengumuman_pengadaan", "result_array");
		$this->smarty->assign("pengumuman_pengadaan",$pengumuman_pengadaan);
		
		
		$this->smarty->display('webpage/pengumuman_pengadaan.php');
	}
	
	public function pengumuman_dpt()
	{
		$tentang_kami=$this->mmodul->getdata ("tentang_kami", "result_array");
		$this->smarty->assign("tentang_kami",$tentang_kami);
		
		$pengumuman_dpt=$this->mmodul->getdata ("pengumuman_dpt", "result_array");
		$this->smarty->assign("pengumuman_dpt",$pengumuman_dpt);
		
		$this->smarty->display('webpage/pengumuman_dpt.php');
	}
	
	public function hasil_pengadaan()
	{
		$tentang_kami=$this->mmodul->getdata ("tentang_kami", "result_array");
		$this->smarty->assign("tentang_kami",$tentang_kami);
		
		$hasil_pengadaan=$this->mmodul->getdata ("hasil_pengadaan", "result_array");
		$this->smarty->assign("hasil_pengadaan",$hasil_pengadaan);
		
		$this->smarty->display('webpage/hasil_pengadaan.php');
	}
	
	public function hasil_dpt()
	{
		$tentang_kami=$this->mmodul->getdata ("tentang_kami", "result_array");
		$this->smarty->assign("tentang_kami",$tentang_kami);
		
		$hasil_dpt=$this->mmodul->getdata ("hasil_dpt", "result_array");
		$this->smarty->assign("hasil_dpt",$hasil_dpt);
		
		$this->smarty->display('webpage/hasil_dpt.php');
	}
	
	public function berita()
	{
		$berita=$this->mmodul->getdata ("berita", "result_array");
		$this->smarty->assign("berita",$berita);
		
		$this->smarty->display('webpage/berita.php');
	}


	public function login()
	{
		// $this->load->view('webpage/tpl_login');
	          
				$this->load->helper(array('captcha','url'));

		      $config_captcha = array(
				'img_path'  => './captcha/',
				'img_url'  => base_url().'captcha/',
				'img_width'  => '200',
				'img_height' => 60,
				'border' => 0,
				'expiration' => 7200
	);

   // create captcha image
   $cap = create_captcha($config_captcha);

   // store image html code in a variable
   $data['img'] = $cap['image'];

   // store the captcha word in a session
    $this->session->set_userdata('mycaptcha', $cap['word']);


		$this->load->view('webpage/tpl_new_login',$data);

	}

	public function register()
	{
	    



    
$data['disable']="disabled";
		// $this->load->view('webpage/tpl_register');
		$this->load->view('webpage/tpl_new_register',$data);

	}

	public function register_sukses()
	{
		$this->load->view('webpage/tpl_register_sukses');
	}
	
	function keterangan()
	{
		
		$table="<table width='100%' border='0'>";
		$table.="<tr><td><font style=\"font-family: arial; font-size: 13px;\"><strong>Barang </strong> adalah setiap benda baik berwujud maupun tidak berwujud, bergerak maupun tidak bergerak, yang dapat
diperdagangkan, dipakai, dipergunakan atau dimanfaatkan oleh pengguna Barang.</td></tr>";
		$table.="<tr><td>&nbsp;</td></tr>";
		$table.="<tr><td><font style=\"font-family: arial; font-size: 13px;\"><strong>Pekerjaan Konstruksi</strong> adalah keseluruhan atau sebagian kegiatan yang meliputi pembangunan, pengoperasian,
pemeliharaan, pembongkaran, dan pembangunan kembali suatu bangunan.</td></tr>";
		$table.="<tr><td>&nbsp;</td></tr>";
		$table.="<tr><td><font style=\"font-family: arial; font-size: 13px;\"><strong>Jasa Konsultansi</strong> adalah jasa layanan profesional yang membutuhkan keahlian tertentu diberbagai bidang keilmuan
yang mengutamakan adanya olah pikir.</td></tr>";
		$table.="<tr><td>&nbsp;</td></tr>";
		$table.="<tr><td><font style=\"font-family: arial; font-size: 13px;\"><strong>Jasa lainnya</strong> adalah jasa non-konsultansi atau jasa yang membutuhkan peralatan, metodologi khusus,dan/atau keterampilan dalam suatu
sistem tata kelola yang telah dikenal luas di dunia usaha untuk menyelesaikan suatu pekerjaan.</td></tr>";
		$table.="</table>";
		
		echo $table;
		//echo "sss";exit;
	}

	function aturan_kondisi()
	{
		$this->load->view('webpage/tpl_aturan_kondisi');
	}


	    public function recaptcha()
    {

         $this->load->library('recaptcha');

        $recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
                echo "You got it!";
            }
        }

        $data = array(
            'widget' => $this->recaptcha->getWidget(),
            'script' => $this->recaptcha->getScriptTag(),
        );
        $this->load->view('recaptcha', $data);
    }
	function reg($p1=""){
		if($p1==""){
			$tipe=$this->mmodul->getdata ("tipe", "result_array");
			$propinsi=$this->mmodul->getdata('propinsi','result_array','get');
			$this->smarty->assign("tipe",$tipe);
			$this->smarty->assign("propinsi",$propinsi);
			$this->smarty->assign("modul",'registrasi.php');
			$this->smarty->display('webpage/tpl_main.php');
		}else{
			$post = array();
			foreach($_POST as $k=>$v){
				if($this->input->post($k)!=""){
					$post[$k] = $this->input->post($k);
				}
				
			}
			echo $this->mmodul->simpan_data("registrasi", $post);
			/*
			//if($p1=="registrasi"){
				if($this->mmodul->simpan_data("registrasi", $post)){
					$subject="Pendaftaran DRT PT. Jasamarga Tollroad Operator Jam  ".date('Y-m-d H:i');
					$isi='
						<!doctype html>
						<html>
						<head>
						<meta charset="UTF-8">
						<title>Untitled Document</title>
						</head>

						<body>
						<blockquote>&nbsp;</blockquote>
						<table width="100%" height="246" border="0">
						  <tbody>
							<tr>
							  <td width="277">&nbsp;<img src="'.$this->host.'__assets/img/logo.png"></td>
							  <td width="81">&nbsp;</td>
							  <td width="11">&nbsp;</td>
							  <td width="442">&nbsp;</td>
							  <td width="224">&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td>Terima Kasih</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="5">
								Anda telah bergabung dengan JMTO e-Procurement <br>
								Untuk mengaktifkan user id anda silahkan klik url ini&nbsp;&nbsp;<a href="'.$this->host.'webpage/Remainder/'.base64_encode($this->input->post('email')).'" target="_blank">'.$this->host.'webpage/Remainder</a>
							  </td>
							</tr>
							
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td height="30" colspan="5">Jika ada pertanyaan lebih lanjut Anda dapat menghubungi helpdesk kami.
							  <br></td>
							</tr>
							<tr>
							  <td colspan="3">HelpDesk E-procurement</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="3">PT. Jasamarga Tollroad Operator</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="5">&nbsp;Gedung Cabang Jagorawi Lt. 4, Plaza Tol Taman Mini Indonesia Indah, 
							  </td>
							</tr>
							<tr>
							  <td colspan="3">Jakarta 13550 Indonesia</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="3">Email : eproc@jmto.co.id</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="3">Telp:(021)22984722</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							
						  </tbody>
						</table>
						</body>
						</html>
						';
					echo $this->lib->kirimemail($this->input->post('email'),$subject,$isi);
				}
			//}else{
			//	echo $this->mmodul->simpan_data("registrasi", $post);
			//}
			*/
		}
	}
	function tes_email(){
		$isi='
						<!doctype html>
						<html>
						<head>
						<meta charset="UTF-8">
						<title>Untitled Document</title>
						</head>

						<body>
						<blockquote>&nbsp;</blockquote>
						<table width="100%" height="246" border="0">
						  <tbody>
							<tr>
							  <td width="277">&nbsp;<img src="'.$this->host.'__assets/img/logo.png"></td>
							  <td width="81">&nbsp;</td>
							  <td width="11">&nbsp;</td>
							  <td width="442">&nbsp;</td>
							  <td width="224">&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td>Terima Kasih</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="5">
								Anda telah bergabung dengan JMTO e-Procurement <br>
								Untuk mengaktifkan user id anda silahkan klik url ini&nbsp;&nbsp;<a href="'.$this->host.'webpage/Remainder/'.base64_encode($this->input->post('email')).'" target="_blank">'.$this->host.'webpage/Remainder</a>
							  </td>
							</tr>
							
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td height="30" colspan="5">Jika ada pertanyaan lebih lanjut Anda dapat menghubungi helpdesk kami.
							  <br></td>
							</tr>
							<tr>
							  <td colspan="3">HelpDesk E-procurement</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="3">PT. Jasamarga Tollroad Operator</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="5">&nbsp;Gedung Cabang Jagorawi Lt. 4, Plaza Tol Taman Mini Indonesia Indah, 
							  </td>
							</tr>
							<tr>
							  <td colspan="3">Jakarta 13550 Indonesia</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="3">Email : eproc@jmto.co.id</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="3">Telp:(021)22984722</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							</tr>
							
						  </tbody>
						</table>
						</body>
						</html>
						';
					echo $this->lib->kirimemail('goyz87@gmail.com','TES',$isi);
	}
	function remainder($p1="",$p2=""){
		//$p1=base64_decode('I2RtMzNuSk1UTyM=');
		//echo $p1;exit;
		//echo base64_decode($p1);exit;
		$ck=$this->db->get_where('drt_peserta',array('email'=>$this->db->escape_str(base64_decode($p1))))->row_array();
		if(isset($ck["email"])){
			$this->smarty->assign("usr",base64_decode($p1));
			$this->smarty->assign("modul",'new_pwd.php');
			$this->smarty->display('webpage/tpl_main.php');
		}else{
			$this->smarty->assign("msg",'Ops User Anda Tidak Terdaftar!!!');
			$this->smarty->display('invalid.html');
		}
		//echo $user;exit;
		//echo uniqid();exit;
	}
	function updt_pwd(){
		$this->db->trans_begin();
		$usr=$this->input->post('email');
		$pwd=$this->input->post('pwd');
		$sql="UPDATE drt_peserta set pass='".$this->encrypt->encode($pwd)."' WHERE email='".$usr."'";
		$this->db->query($sql);
		if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			echo 0;
		}else{
			echo $this->db->trans_commit();	
		}
	}
	function get_combo(){
		$mod=$this->input->post('v');
		$val=$this->input->post('v3');
		$bind=$this->input->post('v2');
		$data=$this->mmodul->getdata($mod,'result_array','get',$bind);
		$opt="<option value=''>--Pilih--</option>";
		
		foreach($data as $v){
			if($v['id']==$val)$sel="selected"; else $sel="";
			$opt .="<option value='".$v['id']."' ".$sel.">".$v['txt']."</option>";
		}
		echo $opt;
	}
	function genCaptcha($rand){
		header("Content-type: image/jpeg");// out out the image
		$RandomStr = md5(microtime());// md5 to generate the random string
		$ResultStr = substr($RandomStr,0,5);//trim 5 digit
		$NewImage = imagecreatefromjpeg("__assets/img/back_captcha.jpg");//image create by existing image and as back ground
		$font='__assets/font/ROCCB.ttf';
		//echo "capcha:".$ResultStr ; exit();
		$TextColor = imagecolorallocate($NewImage, 1, 34, 128);//text color-white
		
		imagettftext($NewImage, 22, 4, 25, 25, $TextColor, $font, $ResultStr);

		$this->session->set_userdata("captcha", $ResultStr);
		imagejpeg($NewImage);//Output image to browser/**/
		ImageDestroy($NewImage); //Free up resources
		exit();
	}
	function checkCaptcha($txt){
		if($txt==$this->captcha){
			echo "sama";
		} else {
			echo "tidak sama";
		}	
	}
}
